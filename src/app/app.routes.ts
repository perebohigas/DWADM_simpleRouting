// Components
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { UserComponent } from './user/user.component';

// Routing
import {RouterModule, Routes} from "@angular/router";

const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'page1', pathMatch: 'full' },
  { path: 'page1', component: Page1Component },
  { path: 'page2', component: Page2Component },
  { path: 'user/:id', component: UserComponent }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
